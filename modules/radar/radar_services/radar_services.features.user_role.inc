<?php

/**
 * @file
 * radar_services.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function radar_services_user_default_roles() {
  $roles = array();

  // Exported role: api user.
  $roles['api user'] = array(
    'name' => 'api user',
    'weight' => 8,
  );

  return $roles;
}
