<?php

/**
 * Ugly mash-up of Services Entity Resource Clean, Services UUID Resource, and
 * custom submission rules.
 */
class RadarServicesEntityResourceController extends ServicesEntityResourceControllerClean {

  protected $apiVersion = '';

  /**
   * Set API version.
   */
  public function setApiVersion($version) {
    $this->apiVersion = $version;
  }

  /**
   * Get API version.
   */
  public function getApiVersion() {
    return $this->apiVersion;
  }

  /**
   * Extends ServicesResourceControllerInterface::access().
   *
   * Default services access does not load by UUID.
   */
  public function access($op, $args) {
    if ($op == 'index') {
      // Access is handled per-entity by index().
      return TRUE;
    }
    // Further restrict Create, Delete and Update operations
    // even if on the website ui it could be otherwise.
    if ($op != 'view' && !user_access('services create update delete operations')) {
      return FALSE;
    }
    if ($op != 'create') {
      // Retrieve, Delete, Update.
      list($entity_type, $entity_uuid) = $args;
      if ($entity_type == 'location' && $op != 'view') {
        services_error(t('If you need to edit locations via the API please contact radar@squat.net'), 403);
      }
      $info = entity_get_info($entity_type);
      $uuid_key = $info['entity keys']['uuid'];
      $ids = entity_get_id_by_uuid($entity_type, [$entity_uuid]);
      $entity_id = reset($ids);

      if (empty($entity_id)) {
        if ($op == 'update') {
          // Create operation with specified UUID. Check $op == 'create'.
          // Sorry this reads like rubbish, but this is the only option here
          // that falls through to the next part of the function the rest return
          // something.
          $op = 'create';
          $args = [$entity_type, $args[2]];
        }
        else {
          services_error(t('Entity not found'), 404);
        }
      }
      else {
        $entity = entity_load_single($entity_type, $entity_id);

        // Pass the entity to the access control.
        return entity_access($op, $entity_type, $entity ? $entity : NULL);
      }
    }
    // For create operations, we need to pass a new entity to entity_access()
    // in order to check per-bundle creation rights. For all other operations
    // we load the existing entity instead.
    if ($op == 'create') {
      list($entity_type, $data) = $args;
      // Workaround for bug in Entity API node access.
      // @todo remove once https://drupal.org/node/1780646 lands.
      if ($entity_type == 'node') {
        return isset($data['type']) ? node_access('create', $data['type']) : FALSE;
      }
      // Create a wrapper from the entity so we can call its access() method.
      $wrapper = $this->createWrapperFromValues($entity_type, $data);
      return $wrapper->entityAccess('create');
    }

  }

  public function create($entity_type, array $values) {
    $this->formSubmissionChecks('create', $entity_type, $values);
    $info = entity_get_info($entity_type);
    $uuid_key = $info['entity keys']['uuid'];
    // Mixing UUID and EntityMetadataWrapper. Need the author already with UID.
    if (!empty($values['author']['id'])) {
      $uids = entity_get_id_by_uuid('user', [$values['author']['id']]);
      $values['author']['id'] = reset($uids);
    }
    $wrapper = $this->createWrapperFromValues($entity_type, $values);

    // Check write access on each property.
    foreach (array_keys($values) as $name) {
      if (!$this->propertyAccess($wrapper, $name, 'create')) {
        services_error(t("Not authorized to set property '@p'", array('@p' => $name)), 403);
      }
    }

    // Make sure that bundle information is present on entities that have
    // bundles. We have to do this after creating the wrapper, because the
    // name of the bundle key may differ from that of the corresponding
    // metadata property (e.g. for taxonomy terms, the bundle key is
    // 'vocabulary_machine_name', while the property is 'vocabulary').
    if ($bundle_key = $wrapper->entityKey('bundle')) {
      $entity = $wrapper->value();
      if (empty($entity->{$bundle_key})) {
        $entity_info = $wrapper->entityInfo();
        if (isset($entity_info['bundles']) && count($entity_info['bundles']) === 1) {
          // If the entity supports only a single bundle, then use that as a
          // default. This allows creation of such entities if (as with ECK)
          // they still use a bundle key.
          $entity->{$bundle_key} = reset($entity_info['bundles']);
        }
        else {
          services_error('Missing bundle: ' . $bundle_key, 406);
        }
      }
    }

    $properties = $wrapper->getPropertyInfo();
    $diff = array_diff_key($values, $properties);
    if (!empty($diff)) {
      services_error('Unknown data properties: ' . implode(' ', array_keys($diff)) . '.', 406);
    }
    $entity = $wrapper->value();
    // Manually adds UUID if required.
    uuid_entity_presave($entity, $entity_type);
    $this->validateFields($entity_type, $entity);
    entity_uuid_save($entity_type, $entity);
    return $this->retrieve($entity_type, $entity->{$uuid_key}, '*', NULL);
  }

  public function update($entity_type, $entity_uuid, array $values) {
    $info = entity_get_info($entity_type);
    $uuid_key = $info['entity keys']['uuid'];
    $values[$uuid_key] = $entity_uuid;
    $ids = entity_get_id_by_uuid($entity_type, [$entity_uuid]);
    $entity_id = reset($ids);
    if (empty($entity_id)) {
      // UUID is not existing, a PUT to create with specified UUID.
      $values[$uuid_key] = $entity_uuid;
      return $this->create($entity_type, $values);
    }

    $this->formSubmissionChecks('update', $entity_type, $values);
    if (!empty($values['author']['id'])) {
      $uids = entity_get_id_by_uuid('user', [$values['author']['id']]); 
      $values['author']['id'] = reset($uids);
    }
    $property_info = entity_get_all_property_info($entity_type);
    $values = $this->transform_values($entity_type, $property_info, $values);
    try {
      $wrapper = entity_metadata_wrapper($entity_type, $entity_id);
      foreach ($values as $name => $value) {
        // Only attempt to set properties when the new value differs from that
        // on the existing entity; otherwise, requests will fail for read-only
        // and unauthorized properties, even if they are not being changed. This
        // allows us to UPDATE a previously retrieved entity without removing
        // such properties from the payload, as long as they are unchanged.
        if (!$this->propertyHasValue($wrapper, $name, $value)) {
          // We set the property before checking access so the new value
          // will be passed to the access callback. This is necesssary in
          // some cases (e.g. text-format fields) where access permissions
          // depend on the value that is being set.
          $wrapper->{$name}->set($value);
          if (!$this->propertyAccess($wrapper, $name, 'update')) {
            services_error(t("Not authorized to set property '@property-name'.", array('@property-name' => $name)), 403);
          }
        }
      }
    }
    catch (EntityMetadataWrapperException $e) {
      services_error($e->getMessage(), 406);
    }
    $entity = $wrapper->value();
    $this->validateFields($entity_type, $entity);
    entity_uuid_save($entity_type, $entity);
    return $this->retrieve($entity_type, $entity->{$uuid_key}, '*', NULL);
  }

 /**
  * Implements ServicesResourceControllerInterface::retrieve().
  *
  * Adds language handling. Uses UUID.
  */
  public function retrieve($entity_type, $entity_id, $fields, $revision, $language = LANGUAGE_NONE) {
    // Load by UUID, and if specified VUUID.
    try {
      if (!empty($revision)) {
        $conditions = array('vuuid' => $revision);
      }
      else {
        $conditions = array();
      }
      $entities = entity_uuid_load($entity_type, array($entity_id), $conditions);
      $entity = reset($entities);

      // Ugly, and quick, workaround. taxonomy_entity_uuid_load() removes the
      // vid. It's needed to localize the term.
      if ($entity_type == 'taxonomy_term') {
        $vocabulary = taxonomy_vocabulary_machine_name_load($entity->vocabulary_machine_name);
        $entity->vid = $vocabulary->vid;
        $entity = i18n_taxonomy_localize_terms($entity);
        unset($entity->vid);
      }
    }
    catch (Exception $exception) {
      watchdog_exception('uuid_services', $exception);
      return services_error($exception, 406, $uuid);
    }
    if (empty($entity)) {
      services_error('Entity or revision not found', 404);
    }

    return $this->format_entity($entity_type, $entity, $fields, $language);
  }

  public function format_entity($entity_type, $entity, $fields, $language) {
    // Users get special treatment to remove sensitive data.
    if ($entity_type == 'user') {
      // Use the helper that Services module already has.
      services_remove_user_data($entity);
    }

    $wrapper = entity_metadata_wrapper($entity_type, $entity);
    if ($language != LANGUAGE_NONE) {
      $wrapper->language($language);
    }

    if ($entity_type == 'node') {
      // If there is an explicit summary set, expose it in addition to the full
      // body in the API.
      $body = $wrapper->body->value();
      $summary = text_summary($body['value'], $body['format'], 0);
      $body['summary'] = $summary == $body['value'] ? '' : $summary;
      $body['safe_summary'] = check_markup($body['summary'], $body['format']);
      $wrapper->body->set($body);
    }

    $data = $this->get_data($wrapper, $fields);

    // Author needs to be on the entity as other fields use it to check access.
    // But we don't display it except to users that can edit it.
    if (!$wrapper->access('edit')) {
      unset($data['author']);
    }

    // Taxonomy terms, and nodes have 'type'. Files not.
    // Drupal 8 style all entities have bundles, even if just one.
    if ($entity_type == 'file') {
      $data['type'] = 'file';
    }

    return $data;
  }

 /**
  * Implements ServicesResourceControllerInterface::delete().
  *
  * Uses UUID.
  */
  public function delete($entity_type, $entity_id) {
    try {
      $return = entity_uuid_delete($entity_type, array($entity_id));
      return $return;
    }
    catch (Exception $exception) {
      watchdog_exception('uuid_services', $exception);
      return services_error($exception, 406, $uuid);
    }
  }

  /**
   * Implements ServicesResourceControllerInterface::index().
   *
   * Adds language handling.
   */
  public function index($entity_type, $fields, $parameters, $page, $pagesize, $sort, $direction, $language = LANGUAGE_NONE) {
    $property_info = entity_get_all_property_info($entity_type);
    $parameters = $this->transform_values($entity_type, $property_info, $parameters);
    $sort = (isset($property_info['field_' . $sort]))?'field_' . $sort:$sort;

    // Call the parent method, which takes care of access control.
    $entities = ServicesEntityResourceController::index($entity_type, '*', $parameters, $page, $pagesize, $sort, $direction);
    foreach($entities as $entity) {
      $wrapper = entity_metadata_wrapper($entity_type, $entity);
      if ($language != LANGUAGE_NONE) {
        $wrapper->language($language);
      }
      $return[] = $this->get_data($wrapper, $fields);
    }
    return $return;
  }

 /**
  * return the data structure for an entity stripped of all "drupalisms" such as
  * field_ and complex data arrays.
  *
  * @param type $wrapper
  * @return type
  */
  protected function get_data($wrapper, $fields = '*') {
    if ($fields != '*') {
      $fields_array = explode(',', $fields);
      $fields_array = array_flip($fields_array);
      foreach ($fields_array as $field_name => $subfields) {
        $fields_array[$field_name] = [];
      }
    }
    if ($fields != '*' && version_compare($this->getApiVersion(), '1.2', '>=')) {
      foreach ($fields_array as $field => $subfields) {
        list($parent_field, $subfield) = explode(':', $field, 2);
        if ($subfield) {
          unset($fields_array[$field]);
          $fields_array[$parent_field][] = $subfield;
        }
      }
    }
    $data = array();
    $filtered = $this->property_access_filter($wrapper);
    foreach ($filtered as $drupal_name => $property) {
      // We don't want 'field_' at the beginning of fields. This is a drupalism and shouldn't be in the api.
      $name = preg_replace('/^field_/', '', $drupal_name);
      // If fields is set and it isn't one of them, go to the next.
      if (!is_int($drupal_name) && $fields != '*' && !isset($fields_array[$name])) {
        continue;
      }
      try {
        if ($property instanceof EntityDrupalWrapper) {
          // For referenced entities only return the URI.
          if ($id = $property->getIdentifier()) {
            $data[$name] = $this->get_resource_reference($property->type(), $id);
            if (version_compare($this->getApiVersion(), '1.2', '>=')) {
              // So in common with some of our other errors the entity won't load
              // here as it's a UUID. Replacing the entity controller would
              // probably do it? For now fix the issue at hand.
              $entity_info = entity_get_info($property->type());
              if (uuid_is_valid($id)) {
                $entities = entity_uuid_load($property->type(), [$id]);
              }
              else {
                $entities = entity_load($property->type(), [$id]);
              }
              $entity_label = empty($entity_info['entity keys']['label']) ? 'label' : $entity_info['entity keys']['label'];
              if (!empty($entities)) {
                $entity = reset($entities);
                // Ugly, and quick, workaround. taxonomy_entity_uuid_load() removes the
                // vid. It's needed to localize the term.
                if ($property->type() == 'taxonomy_term') {
                  $vocabulary = taxonomy_vocabulary_machine_name_load($entity->vocabulary_machine_name);
                  $entity->vid = $vocabulary->vid;
                  $entity = i18n_taxonomy_localize_terms($entity);
                  unset($entity->vid);
                }
                $data[$name][$entity_label] = entity_label($property->type(), $entity);
                if ($fields != '*') {
                  $field_wrapper = entity_metadata_wrapper($property->type(), $entity);
                  $get_fields = is_int($drupal_name) ? $fields : implode(',', $fields_array[$name]);
                  $subfield_data = $this->get_data($field_wrapper, $get_fields);
                  $data[$name] = array_merge($data[$name], $subfield_data);
                }
              }
              else {
                $data[$name][$entity_label] = '';
              }
            }
          }
        }
        elseif ($property instanceof EntityValueWrapper) {
          $data[$name] = $property->value();
        }
        elseif ($property instanceof EntityListWrapper) {
          if (isset($fields_array[$name]) && count($fields_array[$name])) {
            $property_fields = implode(',', $fields_array[$name]);
          }
          else {
            $property_fields = '*';
          }
          $data[$name] = $this->get_data($property, $property_fields);
        }
        elseif ($property instanceof EntityStructureWrapper) {
          if (isset($fields_array[$name]) && count($fields_array[$name])) {
            $property_fields = implode(',', $fields_array[$name]);
          }
          else {
            $property_fields = '*';
          }
          $data[$name] = $this->get_data($property, $property_fields);
        }
      }
      catch (EntityMetadataWrapperException $e) {
        // A property causes problems - ignore that.
      }
    }
    return $data;
  }

  /**
   * Return a resource reference array.
   *
   * @param type $resource
   * @param type $id
   * @return type
   */
  protected function get_resource_reference($resource, $id) {
    $uri = services_resource_uri(array('entity_' . $resource, $id));
    // locale is adding /lang/ to the beginning of the url.
    // TODO investigate how to do this properly for /api/*
    $uri = preg_replace(',([a-z:]+)//([^/]+)/[a-z]{2}/(.*),', '$1//$2/$3', $uri, 1);
    $return = array(
      'uri' => $uri,
      'id' => $id,
      'resource' => $resource,
    );
    if (module_exists('uuid') && $info = entity_get_info($resource) && !empty($info['entity keys']['uuid'])) {
      $ids = entity_get_uuid_by_id($resource, array($id));
      if ($id = reset($ids)) {
        $return['uuid'] = $id;
      }
    }
    return $return;
  }

  /**
   * Filters out properties where view access is not allowed for the current user.
   *
   * @param EntityMetadataWrapper $wrapper
   *   EntityMetadataWrapper that should be checked.
   *
   * @return
   *   An array of properties where access is allowed, keyed by their property
   *   name.
   */
  protected function property_access_filter($wrapper) {
    $filtered = array();
    foreach ($wrapper as $name => $property) {
      try {
        // Special casing file (not) entities.
        // Workaround https://0xacab.org/radar/drupal-make/issues/163
        if ($name == 'file' || $property->access('view')) {
          $filtered[$name] = $property;
        }
      }
      catch (EntityMetaDataWrapperException $e) {
        // Log the exception and ignore the property. This is known to happen
        // when attempting to access the 'book' property of a non-book node.
        // In such cases Entity API erroneously throws an exception.
        // @see https://drupal.org/node/2051087 and linked issues.
        // watchdog('radar_services_entity', 'Exception testing access to property @p: @e', array('@p' => $name, '@e' => $e->getMessage()), WATCHDOG_WARNING);
      }
    }
    return $filtered;
  }

  /**
   * Checks for field_ prefix for each field and adds it if necessary.
   *
   * @param type $values
   * @return type
   */
  protected function transform_values($entity_type, $property_info, $values) {
    foreach ($values as $key => $value) {
      // Stop gap while we still have a field_type.
      if ($key == 'type') {
        continue;
      }
      // Handle Resource references so we can pass pack the object.
      if (is_array($value) && isset($value['id'])) {
        $values[$key] = $value['id'];
      }
      // Also if it's a multiple field.
      if (is_array($value)) {
        foreach ($value as $delta => $item) {
          // Handle Resource references so we can pass pack the object.
          if (is_array($item) && isset($item['id'])) {
            $values[$key][$delta] = $item['id'];
          }
          // Reformat incoming date.
          if (is_array($item) && isset($item['time_start'])) {
            if (empty($item['timezone'])) {
              services_error(t('Timezone required: @key', ['@key' => $key]), 400);
            }
            $date = $item['time_start'];
            $date_match = [];
            if (preg_match('/^(\d{4}-\d{2}-\d{2}).(\d{2}:\d{2}:\d{2})/', $date, $date_match)) {
              $values[$key][$delta]['value'] = $date_match[1] . ' ' . $date_match[2];
            }
            else {
              services_error(t('Date @key time_start required format YYYY-MM-DD HH:MM:SS', ['@key' => $key]), 400);
            }
            if (!empty($item['time_end'])) {
              $date = $item['time_end'];
              $date_match = [];
              if (preg_match('/^(\d{4}-\d{2}-\d{2}).(\d{2}:\d{2}:\d{2})/', $date, $date_match)) {
                $values[$key][$delta]['value2'] = $date_match[1] . ' ' . $date_match[2];
              }
              else {
                services_error(t('Date @key time_end required format YYYY-MM-DD HH:MM:SS', ['@key' => $key]), 400);
              }
            }
            else {
              $values[$key][$delta]['value2'] = $date_match[1] . ' ' . $date_match[2];
            }
          }
        }
      }
      // Check if this is actually a field_ value.
      if (isset($property_info['field_' . $key])) {
        $values['field_' . $key] = $values[$key];
        unset($values[$key]);
      }
    }
    return $values;
  }

  /**
   * Helper function to create a wrapped entity from provided data values.
   *
   * @param $entity_type
   *   The type of entity to be created.
   * @param $values
   *   Array of data property values.
   * @return EntityDrupalWrapper
   *   The wrapped entity.
   * @todo the created wrapper should probably be statically cached, so we
   * don't have to build it twice (first on access() and again on create()).
   */
  protected function createWrapperFromValues($entity_type, array &$values) {
    $property_info = entity_get_all_property_info($entity_type);
    $values = $this->transform_values($entity_type, $property_info, $values);
    try {
      $wrapper = entity_property_values_create_entity($entity_type, $values);
    }
    catch (EntityMetadataWrapperException $e) {
      services_error($e->getMessage(), 406);
    }
    return $wrapper;
  }

  /**
   * Determine whether a wrapper property has a specified value.
   *
   * @param \EntityMetadataWrapper $wrapper
   *   The wrapper whose property is to be checked.
   * @param $name
   *   The name of the property to check.
   * @param mixed $value
   *   The value to compare it to. May be a wrapper, identifier or raw value.
   *
   * @return boolean
   *   TRUE if the property's current value is equal to the given value. FALSE
   *   if they are different.
   */
  protected function propertyHasValue(EntityMetadataWrapper $wrapper, $name, $value) {
    $property = $wrapper->{$name};
    if ($property instanceof EntityDrupalWrapper) {
      if ($value instanceof EntityDrupalWrapper) {
        return $value->getIdentifier() == $property->getIdentifier();
      }
      elseif (is_numeric($value)) {
        return $value == $property->getIdentifier();
      }
    }
    return $value == $property->value();
  }

  // Validation.
  //
  // Various permissions assume access via the form. These need to recreate
  // this:
  //  * groups are unpublished.
  //  * users without moderation permissions can't change owner, status, date on
  //  node.
  //  * og permissions are checked for groups and proposed groups.
  private function formSubmissionChecks($op, $entity_type, &$values) {
    global $user;

    if ($op == 'update') {
      if (!user_access('administer nodes')) {
        unset($values['uid'], $values['author'], $values['name'], $values['created'], $values['revision']);
      }
      if (isset($values['log'])) {
        $values['log'] .= "\nVia API.\n";
      }
      else {
        $values['log'] = 'Via API';
      }

      if (isset($values['og_group_ref']) || isset($values['og_group_requst'])) {
        $info = entity_get_info($entity_type);
        $uuid_key = $info['entity keys']['uuid'];
        $entity_ids = entity_get_id_by_uuid($entity_type, [$values[$uuid_key]]);
        $entity_id = reset($entity_ids);
        $original = entity_metadata_wrapper($entity_type, $entity_id);
        $this->filterUpdatePostingGroups($values['og_group_ref'], $values['og_group_request'], $entity_type, $values['type'], $original);
        if (empty($values['og_group_ref'])) {
          $values['status'] = 0;
        }
      }
    }
    if ($op == 'create') {
      $values['author'] = ['id' => $user->uuid];
      $values['created'] = time();
      if (isset($values['log'])) {
        $values['log'] .= "\nVia API.\n";
      }
      else {
        $values['log'] = 'Via API';
      }
      if (!user_access('administer nodes')) {
        if ($entity_type == 'node') {
          if ($values['type'] == 'event') {
            $this->filterCreatePostingGroups($values['og_group_ref'], $values['og_group_request'], $values['type']);
            if (empty($values['og_group_ref'])) {
              $values['status'] = 0;
            }
          }
          else {
            $values['status'] = 0;
          }
        }
      }
    }
  }

  /**
   * Validates an entity's fields before they are saved.
   */
  protected function validateFields($entity_type, $entity) {
    try {
      $local_entity = clone $entity;
      entity_make_entity_local($entity_type, $local_entity);
      field_attach_validate($entity_type, $local_entity);
    }
    catch (FieldValidationException $e) {
      services_error($e->getMessage(), 422);
    }
  }

  private function filterCreatePostingGroups(&$og_group_ref, &$og_group_request, $type) {
    $post = [];
    $request = [];

    foreach ((array) $og_group_ref as $group) {
      $gids = entity_get_id_by_uuid('node', [$group['id']]);
      $gid = reset($gids);
      if (og_user_access('node', $gid, "create $type content")) {
        $post[] = $group;
      }
      else {
        // Push to see if it can be a request.
        $og_group_request[] = $group;
      }
    }

    foreach ((array) $og_group_request as $group) {
      $gids = entity_get_id_by_uuid('node', [$group['id']]);
      $gid = reset($gids);
      if (radar_og_can_post_request($gid, $type)) {
        $request[] = $group;
      }
    }

    $og_group_ref = $post;
    $og_group_request = $request;
  }

  private function filterUpdatePostingGroups(&$og_group_ref, &$og_group_request, $entity_type, $type, $original) {
    global $user;
    $uid = $user->uid;

    $post = [];
    foreach ((array) $og_group_ref as $group) {
      $nids = entity_get_id_by_uuid($entity_type, [$group['id']]);
      $post[] = reset($nids);
    }
    $request = [];
    foreach ((array) $og_group_request as $group) {
      $nids = entity_get_id_by_uuid($entity_type, [$group['id']]);
      $request[] = reset($nids);
    }

    $new_groups = array_diff($post, $original->og_group_ref->raw());
    $del_groups = array_diff($original->og_group_ref->raw(), $post);
    $new_request = array_diff($request, $original->og_group_request->raw());
    $del_request = array_diff($original->og_group_request->raw(), $request);

    foreach ($new_groups as $gid) {
      if (!og_user_access('node', $gid, "create $type content")) {
        $uuids = entity_get_uuid_by_id('node', [$gid]);
        $uuid = reset($uuids);
        foreach ($og_group_ref as $key => $ref) {
          if ($ref['id'] == $uuid) {
            unset($og_group_ref[$key]);
          }
        }
      }
    }

    if ($original->author->uid != $uid) {
      foreach ($del_groups as $gid) {
        if (!og_user_access('node', $gid, "create $type content")) {
          $uuids = entity_get_uuid_by_id('node', [$gid]);
          $uuid = reset($uuids);
          $og_group_ref[] = ['id' => $uuid];
        }
      }
    }

    foreach ($new_request as $gid) {
      if (!radar_og_can_post_request($gid, $type)) {
        $uuids = entity_get_uuid_by_id('node', [$gid]);
        $uuid = reset($uuids);
        foreach ($og_group_request as $key => $ref) {
          if ($ref['id'] == $uuid) {
            unset($og_group_request[$key]);
          }
        }
      }
    }

    if ($original->author->uid != $uid) {
      foreach ($del_request as $gid) {
        if (!og_user_access('node', $gid, "create $type content")) {
          $uuids = entity_get_uuid_by_id('node', [$gid]);
          $uuid = reset($uuids);
          $og_group_ref[] = ['id' => $uuid];
        }
      }
    }
  }

}
