<?php

/**
* Plugin definition.
*/
$plugin = array(
  'title' => t('Group content node: Add, or propose, to another group'),
  'description' => t('Link to choose additional groups to add or propose.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'category' => t('Organic groups'),
  'defaults' => array(
    'types' => [],
    'field_name' => OG_AUDIENCE_FIELD,
  ),
);

/**
 * Render callback.
 */
function radar_event_og_node_add_group_link_content_type_render($subtype, $conf, $args, $context) {
  if (empty($context->data)) {
    return FALSE;
  }

  $node = $context->data;
  ctools_include('modal');
  ctools_modal_add_js();
  drupal_add_js([
      'CToolsModal' => []
    ], 'setting');

  $links['radar_event_og_link'] = [
    '#type' => 'container',
  ];
  $links['radar_event_og_link']['link'] = [
    '#theme' => 'link',
    '#text' => t('Add other organising groups, add groups that list this event, or remove additional groups.'),
    '#path' => 'node/' . $node->nid . '/event_og/nojs',
    '#options' => [
      'attributes' => ['class' => 'ctools-use-modal'],
      'html' => FALSE,
    ],
  ];
   
  if (!$links) {
    return FALSE;
  }

  $module = 'og';
  $block = new stdClass();
  $block->module = $module;
  $block->title = t('Add groups');

  $block->content = $links;
  return $block;
}
