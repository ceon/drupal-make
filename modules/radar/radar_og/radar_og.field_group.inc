<?php

/**
 * @file
 * radar_og.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function radar_og_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_og_settings|node|group|form';
  $field_group->group_name = 'group_og_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'group';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group access settings',
    'weight' => '10',
    'children' => array(
      0 => 'field_og_subscribe_settings',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-og-settings field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups[''] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Files');
  t('Group access settings');

  return $field_groups;
}
