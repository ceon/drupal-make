<?php

/**
 * Overview of the group memberships (e.g. group manager, total memebrs).
 */
class radar_og_handler_area_create_content_links extends views_handler_area {

  function render($empty = FALSE) {

    // Get arguments.
    foreach ($this->view->argument as $key => $handler) {
      if ($key == 'gid') {
        $gid = $handler = $handler->get_value();
      }
    }
    if (empty($gid)) {
      // No group ID provided.
      return;
    }

    $links = radar_event_og_node_create_links($gid);
    $links['og_node_create_links']['#attributes'] = ['class' => 'button radar-og-create-links'];
    return render($links);
    // return theme('item_list', array('items' => $items, 'title' => t('Group overview')));
  }
}
