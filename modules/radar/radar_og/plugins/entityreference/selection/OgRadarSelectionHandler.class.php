<?php

/**
 * @file
 * OG Radar groups selection handler.
 */

class OgRadarSelectionHandler extends OgSelectionHandler {

  public function getReferencableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    return [];
  }

  public function countReferencableEntities($match = NULL, $match_operator = 'CONTAINS') {
    return;
  }

  public function validateReferencableEntities(array $ids) {
    $auth = [];
    if ($this->field['field_name'] == 'og_group_request') {
      foreach ($ids as $gid) {
        if (radar_event_og_access_propose($gid)) {
          $auth[] = $gid;
        }
      }
    }
    else {
      foreach ($ids as $gid) {
        if (og_user_access('node', $gid, "create event content")) {
          $auth[] = $gid;
        }
      }
    }
    return $auth;
  }

  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    return new self($field, $instance, $entity_type, $entity);
  }

  /**
   * Build an EntityFieldQuery to get referencable entities.
   */
  public function buildEntityFieldQuery($match = NULL, $match_operator = 'CONTAINS') {
    throw new Exception('Radar OG buildEntityFieldQuery was called. It shouldn\'t be anymore. Tell radar@squat.net');

    $handler = EntityReference_SelectionHandler_Generic::getInstance($this->field, $this->instance, $this->entity_type, $this->entity);
    $query = $handler->buildEntityFieldQuery($match, $match_operator);
    return $query;
  }
}
