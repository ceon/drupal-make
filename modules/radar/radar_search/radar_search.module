<?php
/**
 * @file
 * Code for the Radar search feature.
 */

include_once 'radar_search.features.inc';

/**
 * Implements hook_module_implements_alter().
 */
function radar_search_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'search_api_query_alter') {
    $group = $implementations['radar_search'];
    unset($implementations['radar_search']);
    $implementations['radar_search'] = $group;
  }
}

/**
 * Implements hook_permissions().
 */
function radar_search_permission() {
  return array(
    'search content' => array(
      'title' => t('Search content'),
      'description' => t('Replacement for core search permission. Used by services seach api.'),
    ),
  );
}

/**
 * Implements hook_facetapi_url_processors().
 */
function radar_search_facetapi_url_processors() {
  return array(
    'radar_search_pretty_paths' => array(
      'handler' => array(
        'label' => t('Radar pretty paths'),
        'class' => 'FacetapiUrlProcessorPrettyPathsRadar',
      ),
    ),
  );
}

/**
 * Implements hook_hook_info().
 */
function radar_search_hook_info() {
  // We use the same group for all hooks, so save code lines.
  $hook_info = array(
    'group' => 'search_api',
  );
  return array(
    'radar_search_api_alter_callback_info' => $hook_info,
  );
}
 
/**
 * Implements hook_facetapi_searcher_info_alter().
 *
 * Alter search descriptions to add our class in place of pretty_paths.
 *
 * @param array &$searcher_info
 *   The return values of hook_facetapi_searcher_info() implementations.
 */
function radar_search_facetapi_searcher_info_alter(array &$searcher_info) {
  foreach ($searcher_info as &$info) {
    // Activate pretty paths optionally per searcher, as configured.
    $id = 'facetapi_pretty_paths_searcher_' . $info['name'];
    $info['url processor'] = variable_get($id) ? 'radar_search_pretty_paths' : 'standard';
  }
}

function radar_search_facetapi_facet_info_alter(array &$facet_info, array $searcher_info) {
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Just clarify our alteration of pretty paths.
 */
function radar_search_form_facetapi_pretty_paths_admin_form_alter(&$form, $form_state) {
  $form['searcher']['#title'] .= ' ' . t('(Radar altered)');
}

/**
 * Implements hook_search_api_alter_callback_info().
 */
function radar_search_api_alter_callback_info() {
  $callbacks['radar_search_api_interface_translation'] = array(
    'name' => t('Interface translations'),
    'description' => t("Adds any interface translations."),
    'class' => 'RadarSearchApiAlterInterfaceTranslations',
  );
  return $callbacks;
}

/**
 * Lets modules alter a search query before executing it.
 *
 * @param SearchApiQueryInterface $query
 *   The search query being executed.
 */
function radar_search_search_api_query_alter(SearchApiQueryInterface $query) {
  if (array_key_exists('field_active', $query->getOption('search_api_facets'))) {
    // We always add the 'OR' filter for active groups. In addition the user can
    // add the inactive groups.
    $query_filters = $query->getFilter();
    foreach ($query_filters->getFilters() as &$filters) {
      foreach ($filters->getFilters() as &$filter) {
        if ($filter[0] == 'field_active') {
          $active_filters = $filters;
          break 2;
        }
      }
    }
    if (!isset($active_filters)) {
      $active_filters = new SearchApiQueryFilter('OR', ['facet:field_active' => 'facet:field_active']);
      $query_filters->filter($active_filters);
    }
    $active_filters->condition('field_active', '0', '=');
  }
}

/**
 * Implements facet_bonus module's hook_facet_items_alter().
 */
function radar_search_facet_items_alter(&$build, &$settings) {
  if ($settings->facet == "field_active") {
    foreach ($build as $key => $item) {
      if ($item['#indexed_value'] == 1) {
        $build[$key]["#markup"] = t('Include inactive groups');
      }
    }
  }
}

/**
 * Implements hook_views_data_alter()
 */
function radar_search_views_data_alter(&$data) {
  foreach (search_api_index_load_multiple(FALSE) as $index) {
    $key = 'search_api_index_' . $index->machine_name;
    $type_info = search_api_get_item_type_info($index->item_type);
    if ($type_info['entity_type'] == 'node') {
      $table = &$data[$key];
      $table['clone_node'] = array(
        'field' => array(
          'title' => t('Clone link'),
          'help' => t('Provide a simple link to clone the node.'),
          'handler' => 'views_handler_field_node_link_clone',
        ),
      );
    }
  }
}

/**
 * Implements hook_preprocess_page().
 *
 * Adds any selected facets to the page title and description.
 */
function radar_search_preprocess_page(&$vars) {
  $facets_list = radar_search_page_facets();
  if ($facets_list) {
    $title = drupal_set_title();
    if (empty($title)) {
      $title = menu_get_active_title();
    }
    $facets_title = array();
    $facets_meta = array();
    foreach ($facets_list as $field_name => $facet_items) {
      if ($field_name == 'field_active') {
        foreach ($facet_items as $key => $value) {
          if ($value == t('not active anymore')) {
            $facet_items[$key] = t('including inactive');
          }
        }
      }
      $these_items = implode(', ', $facet_items);
      $facets_title[] = $these_items;
      if ($field_name == 'field_category' || $field_name == 'field_topic') {
        $facets_meta['topic'][] = $these_items;
      }
      if ($field_name == 'search_api_aggregation_1') {
        $facets_meta['date'][] = $these_items;
      }
      if ($field_name == 'day') {
        $facets_meta['day'][] = $these_items;
      }
       if ($field_name == 'og_group_ref') {
        $facets_meta['group'][] = $these_items;
      }
      if (substr($field_name, 0, 13) == 'field_offline') {
        $facets_meta['location'][] = $these_items;
      }
    }
    $facets_string = '';
    if (!empty($facets_meta['location'])) {
      $facets_string .= t('in @location', array('@location' => implode(', ', $facets_meta['location']))) . ' ';
    }
    if (!empty($facets_meta['date'])) {
      $facets_string .= t('in @year_or_month', array('@year_or_month' => implode(', ', $facets_meta['date']))) . ' ';
    }
    if (!empty($facets_meta['day'])) {
      $facets_string .= t('on @day', array('@day' => implode(', ', $facets_meta['day']))) . ' ';
    }
    if (!empty($facets_meta['group'])) {
      $facets_string .= t('organised, published or involving @group', array('@group' => implode(', ', $facets_meta['group']))) . ' ';
    }
    if (!empty($facets_meta['topic'])) {
      $facets_string .= ': ' . implode(' - ', $facets_meta['topic']);
    }
    $element = array(
      '#tag' => 'meta',
      '#attributes' => array(
        'name' => 'description',
        'content' => t('Alternative and radical @type @facets_string', array('@type' => strtolower($title), '@facets_string' => $facets_string)),
      ),
    );
    drupal_set_title($title . ': ' . implode(', ', $facets_title));
    drupal_add_html_head($element, 'radar_search:description');
  }
}

/**
 * Retrieve human readable names of the active facets on current page.
 */
function radar_search_page_facets() {
  $facets = array();
  if ($searchers = facetapi_get_active_searchers()) {
    $searcher = reset($searchers);
    $adapter = facetapi_adapter_load($searcher);
    foreach ($adapter->getAllActiveItems() as $item) {
      $markup = $adapter->getMappedValue($item['facets'][0], $item['value']);
      // Special case day.
      if ($item['field alias'] == 'search_api_aggregation_1' && strlen($item['value']) == 10) {
        $facets['day'][] = empty($markup['#html']) ? check_plain($markup['#markup']) : strip_tags($markup['#markup']);
      }
      else {
        $facets[$item['field alias']][] = empty($markup['#html']) ? check_plain($markup['#markup']) : strip_tags($markup['#markup']);
      }
    }
  }
  return $facets;
}

/**
 * Implements hook_block_info() to create a block for the front page.
 */
function radar_search_block_info() {
  $blocks['radar_search_intro_block'] = array(
    'info' => t('Home page introduction text'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view() to add content to the block created above.
 */
function radar_search_block_view() {
  $block['subject'] = '';
  $block['content'] = '<p>' . t('Start searching for events by country, city or category, or filter by <a href="/groups">groups</a>.') . '</p>';
  return $block;
}

/**
 * Implements hook_search_api_alter_callback_info().
 */
function radar_search_search_api_alter_callback_info() {
  $callbacks['radar_search_api_alter_exclude_event_date'] = array(
    'name' => t('Exclude past events'),
    'description' => t('Radar specific. Don\'t keep indexing past events.'),
    'class' => 'RadarSearchApiAlterExcludeEventDate',
  );

  return $callbacks;
}

function radar_search_form_user_login_alter($form, $form_state, $form_id) {
  if (isset($_GET['login_reason']) && $_GET['login_reason'] == 'facets') {
    drupal_set_message(t('Sorry, but in an attempt to combat abuse (bots), we\'ve made it so you have to log in to filter on more than 2 facets.<br />If you don\'t have an account yet creating one is instant, you don\'t need to give personal information, and we don\'t log your IP.'));
  }
}

